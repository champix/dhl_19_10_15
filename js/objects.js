'use strict;'
/**
 * 
 * @param {Object} params 
 */
function parseJSONNote(params) {
    var datas = (undefined !== params.obj) ? params.obj : JSON.parse(params.jsonstr);

    var isArray = Array.isArray(datas);
    if (!isArray) {
        var date = new Date(datas.date);
        delete datas.heure;
        datas.date = date;
        var note = Object.assign(new Note(), datas);
        makeNote(note);
        return note;
    } else {
        var arrayRet = [];
        datas.forEach(element => {
            arrayRet.push(parseJSONNote({ obj: element }));

        });
        return arrayRet;

    }
}

function Note(id, name, date, desc) {
    this.desc = (undefined === desc) ? '' : desc;
    this.date = new Date(date);
    this.name = (undefined === name) ? '' : name;

    if (undefined !== id || '' != id) this.id = id;
    // this.abc = function() {

    // }


}