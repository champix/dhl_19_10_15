'use strict;'



function makeNote(note) {
    var divOfnote = document.createElement('div');
    // divOfnote.classList.add('note', 'shad');
    divOfnote.id = "note-" + note.id;
    divOfnote.innerHTML =
        '<div class="close-container" style="text-align:right;"><img src="img/close.png" style="width:20px;height:20px;"></div><div class="note-date">' + note.date.toDateString() + '&nbsp;' + note.date.toTimeString() +
        '</div> <br/><div > Nom de la note:<span class = "note-name" >' +
        note.name +
        '</span></div><br/><div class = "underline" > contenu: </div> <br /><div class = "note-content" >' +
        note.desc +
        '</div><br></div>';

    divOfnote.querySelector('.close-container img').addEventListener('click', supprNote);
    document.querySelector("#liste-note").append(divOfnote);



}


jsloaded();

function jsloaded(fgd) {
    var blockLoaded = document
        .querySelector("#js-loaded");
    blockLoaded.style.backgroundColor = 'GREEN';
    blockLoaded.innerText = 'Hello le JS file';
    return "everything is OK!!!"
}

function supprNote(sender) {
    var id = sender.target.parentElement.parentElement.id.split('-')[1];

    (new RestCRUD()).delete(id, function(response) { sender.target.parentElement.parentElement.remove(); });


}

function defineEventOnElement(evt) {
    // var noteUn = document.querySelector('#note-1 div.close-container img');
    // noteUn.addEventListener('click', supprNote);

    var buttons = document.querySelectorAll('form+div>button');
    buttons[0].addEventListener('click', resetNoteForm);
    buttons[1].addEventListener('click', submitForm);

    document.querySelector('form select').selectedIndex = -1;
    document
        .forms["form-note"].querySelector('select').addEventListener('change', onchangeselectuser);

    (new RestCRUD()).read();
}
document.body.onload = defineEventOnElement;


function onchangeselectuser(evt) {
    console.log(evt.target);
    if (evt.target.selectedIndex >= 0) {
        var imgpath = 'img/users/' + evt.target.item(evt.target.selectedIndex).value;
    } else {
        var imgpath = 'img/users/default.png';
    }
    document.querySelector('form .note-user-img').src = imgpath;
}

function submitForm(evt) {
    document.querySelector('#edition-note').style.display = 'none';

    var id = document.forms["form-note"]["note-id"].value;
    var date = document.forms["form-note"]["note-date"].value + ' ' + document.forms["form-note"]["note-time"].value;
    var name = document.forms["form-note"]["note-name"].value;
    var desc = document.forms["form-note"]["note-desc"].value;


    var note = new Note(id, name, date, desc);

    (new RestCRUD()).create(note, function(response) {
        parseJSONNote({ jsonstr: response });
    });


    document.forms["form-note"].reset();
    console.log('oki');
}

function resetNoteForm() {
    document.forms[0].reset();
}

// (function(thtr) {
//     console.log(thtr);

// })('jhkjh');



function nxhr() {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:7500/notes', true);
    xhr.onreadystatechange = function(response) {
        if (response.target.readyState == 4 && response.target.status >= 200 && response.target.status < 300) {
            var obj = JSON.parse(response.target.response)

            console.log(response.target.status, response.target.response, obj);


        }
    }
    xhr.send();
}