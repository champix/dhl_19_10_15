//var rstcrd = new RestCRUD('http://localhost:7500', '/notes');
/**
 * 
 * @param {URL} url addr du serveur (sans / de fin)
 * @param {String} ressourceName  nom de ressource commencant par /
 */

const ADR_REST_SRV = 'http://localhost:7500';

function RestCRUD(url, ressourceName) {
    var _self = this;

    if (undefined === url) url = ADR_REST_SRV;
    if (undefined === ressourceName) ressourceName = '/notes';
    const _URL = url + ressourceName;
    var _isTotallyRecieved = false;
    this.isFinished = function() {
        return _isTotallyRecieved;
    };
    this.lastRecievedObject = undefined;
    this.lastStatus = undefined;
    this.lastErrCode; //soit undefined car non affecté;
    /**
     * call function for generic XHR
    
    this.callxhr = _callxhr; */
    /**
     * 
     * @param {*} method 
     * @param {*} ressource 
     */


    function _callxhr(params) { //method, ressourceUrl, obj,callback) {
        var xhr = new XMLHttpRequest();
        xhr.open(params.method, params.ressourceUrl, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function(event) {
            //ver : fin d'execution de la request (Ready state = 4 / DONE ) && dans la plage des 200 pour le status 
            if (this.readyState == this.DONE && (this.status >= 200 && this.status < 300)) {
                // console.log(this.statusText, JSON.parse(this.response));
                if (undefined !== params.callback) params.callback(this.response);

            }
            //Event.target.response

        }

        if ('POST' === params.method) delete params.obj.id;
        xhr.send((undefined !== params.obj) ? JSON.stringify(params.obj) : undefined);
    };


    this.create = function(objet, clbk) {
        console.log('read function called');
        _callxhr({ method: 'POST', ressourceUrl: _URL, obj: objet, callback: clbk });

    }
    this.read = function(id) {
        _isTotallyRecieved = false;
        console.log('read function called');
        _callxhr({
            method: 'GET',
            ressourceUrl: (undefined !== id) ? _URL + '/' + id : _URL,
            callback: function(xhrresponse) {

                _self.lastRecievedObject = parseJSONNote({ jsonstr: xhrresponse });
                _isTotallyRecieved = true;
                console.log('la resquest est fini', _self.lastRecievedObject);
            }
        });

    }
    this.update = function(obj) {
        console.log('read function called');
        _callxhr('PUT', _URL + obj.id);

    }
    this.delete = function(id, clbk) {
        if (undefined === id) return;
        console.log('read function called');
        _callxhr({ method: 'DELETE', ressourceUrl: _URL + '/' + id, callback: clbk });


    }
}